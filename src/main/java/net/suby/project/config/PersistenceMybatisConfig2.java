package net.suby.project.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@MapperScan(value="net.suby.project.user.dao.db2", sqlSessionFactoryRef="myBatisSqlSessionFactory2")
@EnableTransactionManagement
public class PersistenceMybatisConfig2 {

    @Bean(name = "myBatisDataSource2")
    @ConfigurationProperties(prefix = "spring.mybatis2.datasource")
    public DataSource myBatisDataSource2() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "myBatisSqlSessionFactory2")
    public SqlSessionFactory myBatisSqlSessionFactory2(@Qualifier("myBatisDataSource2") DataSource myBatisDataSource2, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(myBatisDataSource2);
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:mapper/user2/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "myBatisSqlSessionTemplate2")
    public SqlSessionTemplate myBatisSqlSessionTemplate2(SqlSessionFactory myBatisSqlSessionFactory2) throws Exception {

        return new SqlSessionTemplate(myBatisSqlSessionFactory2);
    }
}
