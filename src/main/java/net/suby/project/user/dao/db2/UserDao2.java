package net.suby.project.user.dao.db2;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-21.
 */
public interface UserDao2 {
    public HashMap<String, String> findUserId2(HashMap<String, String> parameter);
}
