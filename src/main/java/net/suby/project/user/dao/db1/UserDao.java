package net.suby.project.user.dao.db1;

import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-21.
 */
public interface UserDao {
    public HashMap<String, String> findUserId(HashMap<String, String> parameter);
}
