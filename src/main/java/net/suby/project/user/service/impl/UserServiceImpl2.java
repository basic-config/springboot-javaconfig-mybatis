package net.suby.project.user.service.impl;

import net.suby.project.user.dao.db2.UserDao2;
import net.suby.project.user.service.UserService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-21.
 */
@Service
public class UserServiceImpl2 implements UserService2 {

    @Autowired
    UserDao2 userDao2;

    @Override
    public HashMap<String, String> findUserId(HashMap<String, String> parameter) {
        return userDao2.findUserId2(parameter);
    }
}
