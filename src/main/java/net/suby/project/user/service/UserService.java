package net.suby.project.user.service;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-21.
 */
public interface UserService {

    public HashMap<String, String> findUserId(HashMap<String, String> parameter);
}
