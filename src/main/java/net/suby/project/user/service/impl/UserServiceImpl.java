package net.suby.project.user.service.impl;

import net.suby.project.user.dao.db1.UserDao;
import net.suby.project.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-21.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public HashMap<String, String> findUserId(HashMap<String, String> parameter) {
        return userDao.findUserId(parameter);
    }
}
