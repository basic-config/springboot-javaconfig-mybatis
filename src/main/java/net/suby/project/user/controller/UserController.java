package net.suby.project.user.controller;

import net.suby.project.user.service.UserService;
import net.suby.project.user.service.UserService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-21.
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserService2 userService2;

    @RequestMapping("/")
    public @ResponseBody
    String root_test() throws Exception{
        return "Hello World";
    }

    @RequestMapping("/user")
    public void user(){
        HashMap<String, String> parameterMap = new HashMap<>();
        parameterMap.put("id", "admin");
        HashMap hashMap = userService.findUserId(parameterMap);
        System.out.println(hashMap);

        HashMap hashMap2 = userService2.findUserId(parameterMap);
        System.out.println(hashMap2);
    }
}
